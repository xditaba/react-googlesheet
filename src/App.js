import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Tabletop from "tabletop";

class App extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    // https://medium.com/@ryan.mcnierney/using-react-google-sheets-as-your-cms-294c02561d59
    // https://docs.google.com/spreadsheets/d/1kLbQl1ArSYjbGOdcTBW8T4H2ZEzBg_OFdQ_n5FsFghM/edit#gid=320636122
    Tabletop.init({
      key: "1kLbQl1ArSYjbGOdcTBW8T4H2ZEzBg_OFdQ_n5FsFghM",
      callback: googleData2 => {
        console.log("google sheet data2 --->", googleData2);
        console.table(googleData2);
        this.setState({
          data: googleData2
        });
      },
      simpleSheet: true
    });
  }

  render() {
    const { data } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React + Google Sheets Demo</h1>
        </header>
        <div id="employee-details">
          {data.map(obj => {
            return (
              <div key={obj.employee}>
                <p>{obj.employee}</p>
                <p>{obj.favDog}</p>
                <img
                  alt={obj.favDog}
                  src={obj.img}
                  height="200px"
                  min-width="200px"
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default App;
